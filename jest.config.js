module.exports = {
  transform: {
    '^.+\\.(js)?$': '<rootDir>/jest-preprocess.js',
  },
  // mocks for css modules
  moduleNameMapper: {
    '.+\\.(css|styl|less|sass|scss)$': '<rootDir>/__mocks__/styleMock.js'
  },
  testPathIgnorePatterns: ['node_modules', '.cache'],
}