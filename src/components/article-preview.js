import React from 'react'
import { Link } from 'gatsby'
import get from 'lodash/get'
import Img from 'gatsby-image'

import styles from './article-preview.module.css'

export default ({ article, align }) => {
  // alternate img/content placement order
  const [imgOrder, contentOrder] = align === 'left' ? [styles.left, styles.right] : [styles.right, styles.left] 
  return (
    <div className={styles.preview}>
      <Img alt="" fluid={article.heroImage.fluid} className={imgOrder} />
      <div className={contentOrder} >
        <p className={styles.category}>{get(article, 'category.name')}{' '}{article.readTime && <small>|{' '}{article.readTime} Minute Read</small>}</p>
        <h3 className={styles.previewTitle}>
          <Link to={`/blog/${article.slug}`}>{article.title}</Link>
        </h3>
        {/* <small>{article.publishDate}</small> */}
        <p
          dangerouslySetInnerHTML={{
            __html: article.description.childMarkdownRemark.html,
          }}
        />
        {article.tags && article.tags.map(tag => (
          <p className={`${styles.tag} bg-blue-500`} key={tag}>
            {tag}
          </p>
        ))}
        <p className={styles.author}>{article.author.name}, {article.author.title}</p>
      </div>
    </div>
  )
}