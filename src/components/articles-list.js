import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import styles from './articles-list.module.css'
import get from 'lodash/get'
import ArticlePreview from './article-preview'

const ArticleList = (data) => {
  const posts = get(data, 'allContentfulBlogPost.edges') || []

  return (
    <div className="wrapper">
      <h2 className="section-headline">Recent articles</h2>
      <ul className={styles.articleList}>
        {posts.map(({ node }, i) => {
          return (
            <li key={node.slug}>
              <ArticlePreview article={node} align={i % 2 === 0 ? 'left' : 'right'} />
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default () =>
  <StaticQuery
    query={query}
    render={ArticleList}
  />

export const query = graphql`
  query {
    allContentfulBlogPost(sort: { fields: [publishDate], order: DESC }) {
      edges {
        node {
          title
          slug
          publishDate(formatString: "MMMM Do, YYYY")
          tags
          readTime
          category {
            name
          }
          author {
            name
            title
          }
          heroImage {
            fluid(maxWidth: 350, maxHeight: 196, resizingBehavior: SCALE) {
             ...GatsbyContentfulFluid_tracedSVG
            }
          }
          description {
            childMarkdownRemark {
              html
            }
          }
        }
      }
    }
  }
`
